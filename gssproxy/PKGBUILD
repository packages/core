# Maintainer (Arch): AndyRTR <andyrtr@archlinux.org>
# Contributor (Arch): Mantas Mikulėnas <grawity@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=gssproxy
pkgver=0.8.3
pkgrel=1
pkgdesc="GSSAPI Proxy"
arch=(i686 x86_64)
url="https://github.com/gssapi/gssproxy"
license=('Expat')
depends=('krb5' 'popt' 'ding-libs')
makedepends=('popt' 'libxslt' 'docbook-xsl' 'doxygen')
optdepends=('logger: message logging support')
options=('emptydirs' 'makeflags')
backup=(etc/gssproxy/{gssproxy,24-nfs-server,80-httpd,99-nfs-client}.conf
        etc/gss/mech.d/gssproxy.conf)
source=(https://github.com/gssapi/gssproxy/releases/download/v$pkgver/$pkgname-$pkgver.tar.gz
        gssproxy.initd
        gssproxy.run)
sha512sums=('144b10ec4e19ad2ded5ae57adf1ca311e2fc6c2b97e202eedde69d82c8d50afc0459ac36c7fc5d5290184eb68547a696b33e8a069fde43478104ac26b2c98bc2'
            'c36d646ee1b7c2e172192782c8d54e51ffb13d75c7afbcd7c67872cfa2baf994a2582bb2b423b27e591051ca2e5b3969dc1f26e7f7a277999537d85e624bcab4'
            'cb2346d36e02406635d3f69ead771dc6a7cf87695d923ba1b924abe6a1f8faefa69102d47bc717d6cfc633310157b2af3f6a59ce8863bf01cba8d1e681ed12c9')

build() {
  cd gssproxy-$pkgver
  # make it find bundled verto from krb5 without its own pkg-config file
  export VERTO_CFLAGS="-I/usr/include"
  export VERTO_LIBS="-L/usr/lib -lverto"
  ./configure --prefix=/usr \
    --sysconfdir=/etc  \
    --with-pubconf-path=/etc/gssproxy \
    --localstatedir=/var \
    --without-selinux \
    --with-initscript=none \
    --with-gpp-default-behavior=REMOTE_FIRST
  make
}

check() {
  cd gssproxy-$pkgver
  make test_proxymech
}

package() {
  cd gssproxy-$pkgver
  make DESTDIR=$pkgdir install

  # cleanup empty directories
  rm -rf $pkgdir/usr/include
  rm -rf $pkgdir/usr/share/doc

  # install default config files
  install -m644 examples/gssproxy.conf $pkgdir/etc/gssproxy/gssproxy.conf
  # nfs services
  install -m644 examples/24-nfs-server.conf $pkgdir/etc/gssproxy/24-nfs-server.conf
  install -m644 examples/99-nfs-client.conf $pkgdir/etc/gssproxy/99-nfs-client.conf
  # httpd service / use Hyperbola UID/GID http/33 (by pkg filesystem)
  install -m644 examples/80-httpd.conf $pkgdir/etc/gssproxy/80-httpd.conf
  sed -i -e "s:euid = apache:euid = http:" $pkgdir/etc/gssproxy/80-httpd.conf

  install -Dm644 examples/mech  $pkgdir/etc/gss/mech.d/gssproxy.conf

  # FS#51574
  install -m700 -d $pkgdir/var/lib/gssproxy/rcache

  install -m755 -d $pkgdir/usr/share/licenses/$pkgname
  install -m644 COPYING $pkgdir/usr/share/licenses/$pkgname/

  # OpenRC
  install -Dm755 $srcdir/gssproxy.initd $pkgdir/etc/init.d/gssproxy

  # runit
  install -Dm755 $srcdir/gssproxy.run $pkgdir/etc/sv/gssproxy/run
}
