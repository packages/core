# Maintainer (Arch): Andreas Radke <andyrtr@archlinux.org>
# Contributor (Arch): judd <jvinet@zeroflux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=reiserfsprogs
pkgver=3.6.27
_debver=3.6.27
_debrel=4
pkgrel=1
pkgdesc="Reiserfs utilities"
arch=('i686' 'x86_64')
url="https://www.kernel.org/"
license=('GPL-2')
groups=('base')
depends=('util-linux')
makedepends=('quilt')
source=("https://www.kernel.org/pub/linux/kernel/people/jeffm/reiserfsprogs/v${pkgver}/${pkgname}-${pkgver}.tar."{xz,sign}
        "https://deb.debian.org/debian/pool/main/r/reiserfsprogs/reiserfsprogs_$_debver-$_debrel.debian.tar.xz"
        reiserfsprogs-3.6.27-loff_t.patch)
sha512sums=('bc524aa1ad7f8502238761fd185bfb8473048947bd579e2803c73371928c6b245e0d90bd9d458f4bebcbf163892dbc9f4bae65aec68ebbaa436be7451c13f50c'
            'SKIP'
            '9f64378d0375dbd2f1fe12364e290ed0c6849636e5e488a7076a6e064bf1d488efcc9ce814480b1c41e1457e0c99b548c6b5174b5d74337583c4f19a7c659af4'
            '74332f8acd84a3d0374d6ff9803e63bc78ceb18ec130fef213bf326a6b146b8e4951ac8aee5b7349239716627889febd1c832862ae058a822cc71b0a75333d2f')
validpgpkeys=('F30CE06E667BE129CA3F0B431E7B4B632179E5B2') # Jeffrey Mahoney

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  patch -p1 -i $srcdir/reiserfsprogs-3.6.27-loff_t.patch
  autoreconf -i
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  CFLAGS+=' -fgnu89-inline'
  ./configure --prefix=/usr --sbindir=/sbin
  make
}

check() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make check
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING "${pkgdir}/usr/share/licenses/${pkgname}/COPYING"
}
