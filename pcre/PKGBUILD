# Maintainer (Arch): Sébastien "Seblu" Luttringer
# Contributor (Arch): Allan McRae <allan@archlinux.org>
# Contributor (Arch): Eric Belanger <eric@archlinux.org>
# Contributor (Arch): John Proctor <jproctor@prium.net>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=pcre
pkgver=8.44
_debver=8.39
_debrel=13
pkgrel=3
pkgdesc='A library that implements Perl 5-style regular expressions'
arch=('i686' 'x86_64')
url='https://www.pcre.org/'
license=('Modified-BSD')
depends=('gcc-libs' 'readline' 'bzip2')
makedepends=('quilt')
validpgpkeys=('45F68D54BBE23FB3039B46E59766E084FB0F43D8') # Philip Hazel
source=("https://ftp.pcre.org/pub/pcre/$pkgname-$pkgver.tar.bz2"{,.sig}
        "https://deb.debian.org/debian/pool/main/p/pcre3/pcre3_$_debver-$_debrel.debian.tar.gz")
sha512sums=('f26d850aab5228799e58ac8c2306fb313889332c39e29b118ef1de57677c5c90f970d68d3f475cabc64f8b982a77f04eca990ff1057f3ccf5e19bd137997c4ac'
            'SKIP'
            '1dda1a982ecf1cf888e4f716101e84d99f427cd46874ab4d4116b0bda0852ef5cb7f57cec0edc7cab24c5095431694787dc052dab771621449f7c9d8c2367b86')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/CVE-2017-6004.patch || true
    rm -v debian/patches/Disable_JIT_on_sparc64.patch || true
    rm -v debian/patches/no_jit_x32_powerpcspe.patch || true
    rm -v debian/patches/pcre_info.patch || true
    rm -v debian/patches/pcregrep.1-patch || true
    rm -v debian/patches/pcreposix.patch || true
    rm -v debian/patches/soname.patch || true
    rm -v debian/patches/upstream-fix-for-cve-2017-7186-upstream- || true
    rm -v debian/patches/upstream-patch-fixing-cve-2020-14155.patch || true

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  ./configure \
    --prefix=/usr \
    --enable-unicode-properties \
    --enable-pcre16 \
    --enable-pcre32 \
    --enable-jit \
    --enable-pcregrep-libz \
    --enable-pcregrep-libbz2 \
    --enable-pcretest-libreadline
  make
}

check() {
  cd $pkgname-$pkgver
  make -j1 check
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install

  # move actual libraries to /lib
  install -dm755 "$pkgdir/lib"
  mv $pkgdir/usr/lib/libpcre.so.1* "$pkgdir/lib"
  ln -sf ../../lib/libpcre.so.1 "$pkgdir/usr/lib/libpcre.so"
  install -Dm644 LICENCE "$pkgdir/usr/share/licenses/$pkgname/LICENCE"
}

# vim:set ts=2 sw=2 et:
