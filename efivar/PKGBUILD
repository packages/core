# Maintainer (Arch): David Runge <dvzrv@archlinux.org>
# Contributor (Arch): Tobias Powalowski <tpowa@archlinux.org>
# Contributor (Arch): Keshav Amburay <(the ddoott ridikulus ddoott rat) (aatt) (gemmaeiil) (ddoott) (ccoomm)>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Maintainer: rachad <rachad @ hyperbola . info>

pkgname=efivar
pkgdesc="Tools and libraries to work with EFI variables"
pkgver=37
_debver=37
_debrel=6
pkgrel=2
arch=('i686' 'x86_64')
url="https://github.com/rhboot/efivar"
license=('LGPL-2.1' 'GPL-2')
makedepends=('quilt')
depends=('glibc')
conflicts=('libefivar')
provides=("libefivar=${pkgver}")
source=("https://github.com/rhboot/efivar/archive/refs/tags/$pkgver.tar.gz"
        "https://deb.debian.org/debian/pool/main/e/efivar/efivar_$_debver-$_debrel.debian.tar.xz")
validpgpkeys=('B00B48BC731AA8840FED9FB0EED266B70F4FEF10') # Peter Jones <pjones@redhat.com>
sha512sums=('eeec740f57448d8f02eddeb920c9e632d18998a97c602abe71b44b69fafd6f4d21514b23ea1941d3f37dd2ae03995f23ac17f7c7e95954cb924a89717c4cf26b'
            '3b4352ea5eb5a64de67e062ab606e17725b1f694b652353cb7d82165499be8ffbedfe3faba29601ecf5679364189f90fe50419e6c38c9e01702599c89756db0f')

prepare() {
  cd "${pkgname}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  # -Werror, not even once
  sed -e 's/-Werror//g' -i gcc.specs
  # remove insecure rpath in efivar-tester
  sed 's|-rpath,$(TOPDIR)/src|-rpath,$(libdir)|g' -i src/test/Makefile
}

build() {
  cd "${pkgname}-${pkgver}"
  make libdir="/usr/lib/" \
       bindir="/usr/sbin/" \
       mandir="/usr/share/man/" \
       includedir="/usr/include/"
  # build efivar-tester
  make libdir="/usr/lib/" \
       bindir="/usr/sbin/" \
       mandir="/usr/share/man/" \
       includedir="/usr/include/" \
       -C src/test
}

package() {
  cd "${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}/" \
       libdir="/usr/lib/" \
       bindir="/usr/sbin/" \
       mandir="/usr/share/man/" \
       includedir="/usr/include/" install -j1 V=1
  install -vDm 755 "src/test/tester" "${pkgdir}/usr/sbin/efivar-tester"
  install -vDm 644 {README.md,TODO} -t "${pkgdir}/usr/share/doc/${pkgname}"
  install -vDm 644 debian/copyright COPYING -t $pkgdir/usr/share/licenses/$pkgname/
}
