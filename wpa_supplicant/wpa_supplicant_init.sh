#!/bin/bash
# Copyright (c) 2009 Roy Marples <roy@marples.name>
# Copyright (c) 2019 Hyperbola Project
# All rights reserved. Released under the 2-clause BSD license.

. /etc/conf.d/wpa_supplicant
: ${wpa_supplicant_conf:=/etc/wpa_supplicant/wpa_supplicant.conf}
wpa_supplicant_if=${wpa_supplicant_if:+-i}$wpa_supplicant_if
command_args="-P /run/wpa_supplicant.pid $wpa_supplicant_args -B -c $wpa_supplicant_conf $wpa_supplicant_if"

find_wireless()
{
	local iface=

	case "$(uname -s)" in
	Linux)
		while true; do
			for iface in /sys/class/net/*; do
				if [ -e "$iface"/wireless -o \
					-e "$iface"/phy80211 ]
				then
					echo "${iface##*/}"
					return 0
				fi
			done
			sleep 10
			[ -z iface ] && continue
		done
		;;
	*)
		while true; do
			for iface in /dev/net/* $(ifconfig -l 2>/dev/null); do
				if ifconfig "${iface##*/}" 2>/dev/null | \
					grep -q "[ ]*ssid "
				then
					echo "${iface##*/}"
					return 0
				fi
			done
			sleep 10
			[ -z iface ] && continue
		done
		;;
	esac

	return 1
}

append_wireless()
{
	local iface= i=

	iface=$(find_wireless)
	if [ -n "$iface" ]; then
		for i in $iface; do
			command_args="$command_args -i$i"
		done
	else
		echo "Could not find a wireless interface"
	fi
}

start()
{
	case " $command_args" in
	*" -i"*) ;;
	*) append_wireless;;
	esac
}

start && wpa_supplicant $command_args &> /dev/null
