# Maintainer (Arch): Andreas Radke <andyrtr@archlinux.org>
# Contributor (Arch): Tom Newsom <Jeepster@gmx.co.uk>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgbase="sqlite"
pkgname=('sqlite' 'sqlite-tcl' 'sqlite-analyzer')
_srcver=3320300
_docver=${_srcver}
pkgver=3.32.3
_debver=3.32.3
_debrel=1
pkgrel=2
pkgdesc="A C library that implements an SQL database engine"
arch=('i686' 'x86_64')
license=('Public-Domain')
url="https://www.sqlite.org/index.html"
depends=('tcl' 'readline' 'zlib')
makedepends=('quilt')
source=(https://www.sqlite.org/2020/sqlite-src-${_srcver}.zip
        https://deb.debian.org/debian/pool/main/s/sqlite3/sqlite3_$_debver-$_debrel.debian.tar.xz
        license.txt)
options=('!emptydirs' '!makeflags') # json extensions breaks parallel build
sha512sums=('7e027c7163a760fb9b6dbfd2e4ccffb39d4972280583fb0d7f8390017101dfed3b12a36653b3130a548ae4d04c6abb11c0a1921b6f09c54c6ae1e72fbdcb2fd4'
            '9d382c4744b5e2f657b4295886ff9917f42d45d39d3a7c198ada99a21cbfd225f6c24ce2d63d6438e38f0d3ebef40241feb59e4f2c600e1f4dea4b6091c43f21'
            'a2e3294c5a8188a0c81d0cde13b6bc33a69b525cb101fb08e5d14ad7dce8342ddd170ef14306ae2adc5107f8b8774f0b20816e69294b6bed1d26473975b8a780')

prepare() {
  cd sqlite-src-$_srcver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/02-use-packaged-lempar.c.patch || true
    rm -v debian/patches/20-hurd-locking-style.patch || true
    rm -v debian/patches/30-cross.patch || true
    rm -v debian/patches/32-fix-readline-path-3.patch || true

    quilt push -av
  fi
#  autoreconf -vfi
}

build() {
  export CPPFLAGS="$CPPFLAGS -DSQLITE_ENABLE_COLUMN_METADATA=1 \
                             -DSQLITE_ENABLE_UNLOCK_NOTIFY \
                             -DSQLITE_ENABLE_DBSTAT_VTAB=1 \
                             -DSQLITE_ENABLE_FTS3_TOKENIZER=1 \
                             -DSQLITE_SECURE_DELETE \
                             -DSQLITE_MAX_VARIABLE_NUMBER=250000 \
                             -DSQLITE_MAX_EXPR_DEPTH=10000"

  # build sqlite
  cd sqlite-src-$_srcver
  ./configure --prefix=/usr \
	--disable-static \
	--disable-amalgamation \
	--enable-fts3 \
	--enable-fts4 \
	--enable-fts5 \
	--enable-rtree \
	--enable-json1 \
	TCLLIBDIR=/usr/lib/sqlite$pkgver
  make
  # build additional tools
  make showdb showjournal showstat4 showwal sqldiff sqlite3_analyzer
}

package_sqlite() {

 pkgdesc="A C library that implements an SQL database engine"
 depends=('readline' 'zlib')
 provides=("sqlite3=$pkgver")
 replaces=("sqlite3")

  cd sqlite-src-$_srcver
  make DESTDIR=${pkgdir} install

  install -m755 showdb showjournal showstat4 showwal sqldiff ${pkgdir}/usr/bin/

  # install manpage
  install -m755 -d ${pkgdir}/usr/share/man/man1
  install -m644 sqlite3.1 ${pkgdir}/usr/share/man/man1/

  # install license
  install -D -m644 ${srcdir}/license.txt ${pkgdir}/usr/share/licenses/${pkgname}/license.txt

  # split out tcl extension
  mkdir $srcdir/tcl
  mv $pkgdir/usr/lib/sqlite* $srcdir/tcl
}

package_sqlite-tcl() {

 pkgdesc="sqlite Tcl Extension Architecture (TEA)"
 depends=('sqlite')
 provides=("sqlite3-tcl=$pkgver")
 replaces=("sqlite3-tcl")

  install -m755 -d ${pkgdir}/usr/lib
  mv $srcdir/tcl/* ${pkgdir}/usr/lib

  # install manpage
  install -m755 -d ${pkgdir}/usr/share/man/mann
  install -m644 ${srcdir}/sqlite-src-$_srcver/autoconf/tea/doc/sqlite3.n ${pkgdir}/usr/share/man/mann/

  # install license
  install -D -m644 ${srcdir}/license.txt ${pkgdir}/usr/share/licenses/${pkgname}/license.txt
}

package_sqlite-analyzer() {

 pkgdesc="An analysis program for sqlite3 database files"
 depends=('sqlite' 'tcl')

  cd sqlite-src-$_srcver
  install -m755 -d ${pkgdir}/usr/bin
  install -m755 sqlite3_analyzer ${pkgdir}/usr/bin/

  # install license
  install -D -m644 ${srcdir}/license.txt ${pkgdir}/usr/share/licenses/${pkgname}/license.txt
}
