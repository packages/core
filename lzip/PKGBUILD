# Maintainer (Arch): Jiachen Yang <farseerfc@gmail.com>
# Maintainer (Arch): apophys <admin AT kubikmilan DOT sk>
# Contributor (Arch): Christoph Zeiler <archNOSPAM_at_moonblade.dot.org>
# Contributor (Arch): Renzo Carbonara <gnuk0001/gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Maintainer: rachad <rachad @ hyperbola . info>

pkgname=lzip
pkgver=1.22
_debver=1.22
_debrel=3
pkgrel=1
pkgdesc="A lossless file compressor based on the LZMA algorithm"
arch=('i686' 'x86_64')
url="https://www.nongnu.org/lzip/lzip.html"
license=('GPL-2')
depends=('gcc-libs')
makedepends=('quilt')
source=("https://download.savannah.gnu.org/releases/$pkgname/$pkgname-$pkgver.tar.gz"{,.sig}
        "https://deb.debian.org/debian/pool/main/l/lzip/lzip_$_debver-$_debrel.debian.tar.xz")
validpgpkeys=('1D41C14B272A2219A739FA4F8FE99503132D7742')  # Antonio Diaz Diaz <antonio@gnu.org>
sha512sums=('318de74effdbdfa79070d28919459a85bf1efe662b818b8af2a3daa964b6c24e3386c37de360c6d5b8b624d549b5c9ed777c6234561129c477e03d92ac2db206'
            'SKIP'
            'b0ca205b6f5864599883d1de1bd4e3cc5ab5da68ec4fb6f15b871e9936c5bda963b306e19047e92881bbe3c2c9e62b7e3779774f5cca58c216488b6e252b3450')

prepare() {
  cd "$srcdir/$pkgname-$pkgver"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$srcdir/$pkgname-$pkgver"
  ./configure --prefix=/usr
  make
}

package() {
  cd "$srcdir/$pkgname-$pkgver"
  make DESTDIR="$pkgdir" install{,-man}
  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
