# Contributor (Arch): Ionut Biru <ibiru@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Jesús E. <heckyel@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=p11-kit
pkgver=0.23.21
_debver=$pkgver
_debrel=2
pkgrel=3
pkgdesc="Provides a way to load and enumerate PKCS#11 modules"
arch=(i686 x86_64)
url='https://p11-glue.github.io/p11-glue/p11-kit.html'
license=(Modified-BSD)
depends=(libtasn1 libffi)
makedepends=(quilt)
source=("https://github.com/p11-glue/p11-kit/releases/download/$pkgver/p11-kit-${pkgver}.tar.xz"{,.sig}
        "https://repo.hyperbola.info:50000/sources/${pkgname}/p11-kit_${_debver}-${_debrel}.debian.tar.xz"{,.sig}
        "0001-Build-and-install-libnssckbi-p11-kit.so.patch")
sha512sums=('4c796ca2c72a650f105a7a70aa62e55edb12e1c151e91ef92bfeee6c5c68982b36023400b42c4efcb1d351b7848e8618c26607cdb0f77b48ae40e2ecfd713e3e'
            'SKIP'
            '67dc26c41a94021b9772cf403da99393d65703ba62863e16a878c0ac12f66a5932c9d6aa31fa93b484303dbc15195ae6817223277a645bb1176fd4588c11901d'
            'SKIP'
            'add69c819fd3d965c620af30b430715df25bf2b66da907a843e79fcfb10f8de6c49ce21a0819db845f44d258a7d420dd43483b85f27a79136c66a6997d0fa87b')
validpgpkeys=('462225C3B46F34879FC8496CD605848ED7E69871'  # Daiki Ueno <ueno@unixuser.org>
              'C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd $pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/30_nogetauxvalonbsd.diff || true
    rm -v debian/patches/35_hurd_enable_secure.diff || true
    rm -v debian/patches/41_kfreebsd_LOCAL_PEERCRED.diff || true

    quilt push -av
  fi

  # Build and install an additional library (libnssckbi-p11-kit.so) which
  # is a copy of p11-kit-trust.so but uses the same label for root certs as
  # libnssckbi.so ("Builtin Object Token" instead of "Default Trust")
  # https://bugs.freedesktop.org/show_bug.cgi?id=66161
  patch -Np1 -i $srcdir/0001-Build-and-install-libnssckbi-p11-kit.so.patch
}

build() {
  cd $pkgname-$pkgver

  ./configure --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --with-module-path=/usr/lib/pkcs11 \
    --with-trust-paths=/etc/ca-certificates/trust-source:/usr/share/ca-certificates/trust-source
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"

  # we don't support gtk-doc
  rm -rf $pkgdir/usr/share/gtk-doc

  ln -srf "$pkgdir/usr/bin/update-ca-trust" "$pkgdir/usr/libexec/p11-kit/trust-extract-compat"
}