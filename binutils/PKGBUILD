# Maintainer (Arch): Allan McRae <allan@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

# toolchain build order: linux-libre-lts-api-headers->glibc->binutils->gcc->binutils->glibc

pkgname=binutils
pkgver=2.34
_debver=2.34
_debrel=8
pkgrel=2
pkgdesc="A set of programs to assemble and manipulate binary and object files"
arch=('i686' 'x86_64')
url="https://www.gnu.org/software/binutils/"
license=('GPL-3' 'LGPL-3')
groups=('base-devel')
depends=('zlib')
makedepends=('quilt')
checkdepends=('dejagnu' 'bc')
conflicts=('binutils-multilib')
replaces=('binutils-multilib')
options=('staticlibs' '!distcc' '!ccache')
source=(https://ftp.gnu.org/gnu/binutils/binutils-$pkgver.tar.lz{,.sig}
        https://deb.debian.org/debian/pool/main/b/binutils/binutils_$_debver-$_debrel.debian.tar.xz)
validpgpkeys=('3A24BC1E8FB409FA9F14371813FCEF89DD9E3C4F') # Nick Clifton <nickc@redhat.com>
sha512sums=('f4aadea1afa85d9ceb7be377afab9270a42ab0fd1fae86a7c69510b80de1aaac76f15cfb8730f9d233466a89fd020ab7e6e705e754c6b40f5fe2d16a5214562e'
            'SKIP'
            'd5b9f3e503ed710ef9618561c0a0cfc91168a3ebbc497a7ecc632b0275f8dbe6e69a9b4ac3fb20c584689f6c54d89662eb72f3c8f56b13e9a123f34a68fd3740')

prepare() {
  cd binutils-$pkgver

  if [[ $pkgver = $_debver ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/002_gprof_profile_arcs.patch || true
    rm -v debian/patches/003_gprof_see_also_monitor.patch || true
    rm -v debian/patches/014_hash_style-both.patch || true
    rm -v debian/patches/014_hash_style-gnu.patch || true
    rm -v debian/patches/127_x86_64_i386_biarch.patch || true
    rm -v debian/patches/128_ppc64_powerpc_biarch.patch || true
    rm -v debian/patches/129_multiarch_libpath.patch || true
    rm -v debian/patches/135_bfd_soversion.patch || true
    rm -v debian/patches/136_bfd_pic.patch || true
    rm -v debian/patches/158_ld_system_root.patch || true
    rm -v debian/patches/161_gold_dummy_zoption.diff || true
    rm -v debian/patches/164_ld_doc_remove_xref.diff || true
    rm -v debian/patches/aarch64-libpath.diff || true
    rm -v debian/patches/branch-updates.diff || true
    rm -v debian/patches/branch-version.diff || true
    rm -v debian/patches/gold-mips.diff || true
    rm -v debian/patches/gprof-build.diff || true
    rm -v debian/patches/libctf-soname.diff || true
    rm -v debian/patches/mips64-default-n64.diff || true
    rm -v debian/patches/ppc-lib-search-order.diff || true

    quilt push -av
  fi

  # hack! - libiberty configure tests for header files using "$CPP $CPPFLAGS"
  sed -i "/ac_cpp=/s/\$CPPFLAGS/\$CPPFLAGS -O2/" libiberty/configure

  mkdir ${srcdir}/binutils-build
}

build() {
  cd binutils-build

  ../binutils-$pkgver/configure \
    --prefix=/usr \
    --with-lib-path=/lib:/usr/lib:/usr/local/lib \
    --with-bugurl=https://issues.hyperbola.info/ \
    --enable-threads \
    --enable-shared \
    --enable-ld=default \
    --enable-gold \
    --enable-plugins \
    --enable-deterministic-archives \
    --with-pic \
    --disable-werror \
    --disable-gdb

  # check the host environment and makes sure all the necessary tools are available
  make configure-host

  make tooldir=/usr
}

check() {
  cd binutils-build

  # unset LDFLAGS as testsuite makes assumptions about which ones are active
  # ignore failures in gold testsuite...
  make -k LDFLAGS="" check || true
}

package() {
  cd binutils-build
  make prefix=${pkgdir}/usr tooldir=${pkgdir}/usr install

  # Remove unwanted files
  rm ${pkgdir}/usr/share/man/man1/{dlltool,windres,windmc}*

  # No shared linking to these files outside binutils
  rm ${pkgdir}/usr/lib/lib{bfd,opcodes}.so
  echo "INPUT ( /usr/lib/libbfd.a -liberty -lz -ldl )" > "$pkgdir"/usr/lib/libbfd.so
  echo "INPUT ( /usr/lib/libopcodes.a -lbfd )" > "$pkgdir"/usr/lib/libopcodes.so

  # install license files
  install -dm755 ${pkgdir}/usr/share/licenses/${pkgname}
  install -m644 ${srcdir}/binutils-$pkgver/COPYING3{,.LIB} ${pkgdir}/usr/share/licenses/${pkgname}
}
