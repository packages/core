# Maintainer (Arch): Sébastien Luttringer <seblu@archlinux.org>
# Contributor (Arch): Allan McRae <allan@archlinux.org>
# Contributor (Arch): Andreas Radke <andyrtr@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=diffutils
pkgver=3.7
_debver=3.7
_debrel=3
pkgrel=2
pkgdesc='Utility programs used for creating patch files'
arch=('i686' 'x86_64')
url='https://www.gnu.org/software/diffutils/'
license=('GPL-3')
groups=('base')
depends=('sh')
makedepends=('quilt')
validpgpkeys=('155D3FC500C834486D1EEA677FD9FCCB000BEEEE') # Jim Meyering
source=("https://ftp.gnu.org/gnu/$pkgname/$pkgname-$pkgver.tar.xz"{,.sig}
        "https://deb.debian.org/debian/pool/main/d/diffutils/diffutils_$_debver-$_debrel.debian.tar.xz")
sha512sums=('7b12cf8aea1b9844773748f72272d9c6a38adae9c3c3a8c62048f91fb56c60b76035fa5f51665dceaf2cfbf1d1f4a3efdcc24bf47a5a16ff4350543314b12c9c'
            'SKIP'
            '3ad00b292abf5d61fdcf71847ddf7f2b3d5febb7ab5a15f37411e369999447615bc36719f9f626b096aea81ea219f6c9015b9f46c7781c935eb713704e3946dd')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr
  make
}

check() {
  cd $pkgname-$pkgver
  make check
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING $pkgdir/usr/share/licenses/$pkgname/COPYING
}

# vim:set ts=2 sw=2 et:
