diff --git a/Makefile.in b/Makefile.in
index 41de94e..efbc84f 100644
--- a/Makefile.in
+++ b/Makefile.in
@@ -125,8 +125,8 @@ bin_PROGRAMS = $(am__EXEEXT_1) $(am__EXEEXT_2) $(am__EXEEXT_3) \
 	$(am__EXEEXT_4) $(am__EXEEXT_5) $(am__EXEEXT_6) \
 	$(am__EXEEXT_7) $(am__EXEEXT_8) $(am__EXEEXT_9) \
 	$(am__EXEEXT_10) $(am__EXEEXT_11) $(am__EXEEXT_12) \
-	$(am__EXEEXT_13)
-sbin_PROGRAMS = $(am__EXEEXT_36) $(am__EXEEXT_37) $(am__EXEEXT_38) \
+	$(am__EXEEXT_13) $(am__EXEEXT_38)
+sbin_PROGRAMS = $(am__EXEEXT_36) $(am__EXEEXT_37) \
 	$(am__EXEEXT_39) $(am__EXEEXT_40) $(am__EXEEXT_41) \
 	$(am__EXEEXT_42) $(am__EXEEXT_43) $(am__EXEEXT_44) \
 	$(am__EXEEXT_45) $(am__EXEEXT_46) $(am__EXEEXT_47) \
@@ -409,7 +409,7 @@ check_PROGRAMS = test_blkdev$(EXEEXT) test_canonicalize$(EXEEXT) \
 @BUILD_LOGIN_TRUE@@HAVE_SELINUX_TRUE@am__append_148 = -lselinux
 @BUILD_LOGIN_TRUE@@HAVE_ECONF_TRUE@am__append_149 = -leconf
 @BUILD_NOLOGIN_TRUE@am__append_150 = nologin
-@BUILD_NOLOGIN_TRUE@am__append_151 = login-utils/nologin.8
+@BUILD_NOLOGIN_TRUE@am__append_151 = login-utils/nologin.1
 @BUILD_UTMPDUMP_TRUE@am__append_152 = utmpdump
 @BUILD_UTMPDUMP_TRUE@am__append_153 = login-utils/utmpdump.1
 @BUILD_CHFN_CHSH_TRUE@am__append_154 = chfn chsh
@@ -4170,7 +4170,7 @@ am__dist_noinst_DATA_DIST = lib/terminal-colors.d.5 libuuid/man/uuid.3 \
 	term-utils/agetty.8 term-utils/setterm.1 term-utils/mesg.1 \
 	term-utils/wall.1 term-utils/write.1 login-utils/last.1 \
 	login-utils/lastb.1 login-utils/sulogin.8 login-utils/login.1 \
-	login-utils/nologin.8 login-utils/utmpdump.1 \
+	login-utils/nologin.1 login-utils/utmpdump.1 \
 	login-utils/chfn.1 login-utils/chsh.1 login-utils/su.1 \
 	login-utils/runuser.1 login-utils/newgrp.1 \
 	login-utils/lslogins.1 login-utils/vigr.8 login-utils/vipw.8 \
diff --git a/login-utils/Makemodule.am b/login-utils/Makemodule.am
index 3a1c272..bca1294 100644
--- a/login-utils/Makemodule.am
+++ b/login-utils/Makemodule.am
@@ -61,8 +61,8 @@ endif # BUILD_LOGIN
 
 
 if BUILD_NOLOGIN
-sbin_PROGRAMS += nologin
-dist_man_MANS += login-utils/nologin.8
+bin_PROGRAMS += nologin
+dist_man_MANS += login-utils/nologin.1
 nologin_SOURCES = login-utils/nologin.c
 endif
 
diff --git b/login-utils/nologin.1 b/login-utils/nologin.1
new file mode 100644
index 0000000..a95ff2b
--- /dev/null
+++ b/login-utils/nologin.1
@@ -0,0 +1,74 @@
+.TH NOLOGIN 8 "November 2019" "util-linux" "System Administration"
+.SH NAME
+nologin \- politely refuse a login
+.SH SYNOPSIS
+.B nologin
+.RB [ \-V ]
+.RB [ \-h ]
+.SH DESCRIPTION
+.B nologin
+displays a message that an account is not available and exits non-zero.  It is
+intended as a replacement shell field to deny login access to an account.
+.PP
+If the file /etc/nologin.txt exists, nologin displays its contents to the
+user instead of the default message.
+.PP
+The exit status returned by
+.B nologin
+is always 1.
+.SH OPTIONS
+\fB\-c\fR, \fB\-\-command\fR \fIcommand\fR
+.br
+\fB\-\-init-file\fR
+.br
+\fB\-i\fR \fB\-\-interactive\fR
+.br
+\fB\-\-init-file\fR \fIfile\fR
+.br
+\fB\-i\fR, \fB\-\-interactive\fR
+.br
+\fB\-l\fR, \fB\-\-login\fR
+.br
+\fB\-\-noprofile\fR
+.br
+\fB\-\-norc\fR
+.br
+\fB\-\-posix\fR
+.br
+\fB\-\-rcfile\fR \fIfile\fR
+.br
+\fB\-r\fR, \fB\-\-restricted\fR
+.IP
+These shell command-line options are ignored to avoid nologin error.
+.IP "\fB\-h\fR, \fB\-\-help\fR"
+Display help text and exit.
+.IP "\fB\-V\fR, \fB\-\-version\fR"
+Display version information and exit.
+.SH NOTES
+.B nologin
+is a per-account way to disable login (usually used for system accounts like http or ftp).
+.BR nologin (8)
+uses /etc/nologin.txt as an optional source for a non-default message, the login
+access is always refused independently of the file.
+.PP
+.BR pam_nologin (8)
+PAM module usually prevents all non-root users from logging into the system.
+.BR pam_nologin (8)
+functionality is controlled by /var/run/nologin or the /etc/nologin file.
+.SH HISTORY
+The
+.B nologin
+command appeared in 4.4BSD.
+.SH AUTHORS
+.UR kzak@redhat.com
+Karel Zak
+.UE
+.SH SEE ALSO
+.BR login (1),
+.BR passwd (5),
+.BR pam_nologin (8)
+.SH AVAILABILITY
+The nologin command is part of the util-linux package and is available from
+.UR https://\:www.kernel.org\:/pub\:/linux\:/utils\:/util-linux/
+Linux Kernel Archive
+.UE .
diff --git a/login-utils/nologin.8 a/login-utils/nologin.8
deleted file mode 100644
index a95ff2b..0000000
--- a/login-utils/nologin.8
+++ /dev/null
@@ -1,74 +0,0 @@
-.TH NOLOGIN 8 "November 2019" "util-linux" "System Administration"
-.SH NAME
-nologin \- politely refuse a login
-.SH SYNOPSIS
-.B nologin
-.RB [ \-V ]
-.RB [ \-h ]
-.SH DESCRIPTION
-.B nologin
-displays a message that an account is not available and exits non-zero.  It is
-intended as a replacement shell field to deny login access to an account.
-.PP
-If the file /etc/nologin.txt exists, nologin displays its contents to the
-user instead of the default message.
-.PP
-The exit status returned by
-.B nologin
-is always 1.
-.SH OPTIONS
-\fB\-c\fR, \fB\-\-command\fR \fIcommand\fR
-.br
-\fB\-\-init-file\fR
-.br
-\fB\-i\fR \fB\-\-interactive\fR
-.br
-\fB\-\-init-file\fR \fIfile\fR
-.br
-\fB\-i\fR, \fB\-\-interactive\fR
-.br
-\fB\-l\fR, \fB\-\-login\fR
-.br
-\fB\-\-noprofile\fR
-.br
-\fB\-\-norc\fR
-.br
-\fB\-\-posix\fR
-.br
-\fB\-\-rcfile\fR \fIfile\fR
-.br
-\fB\-r\fR, \fB\-\-restricted\fR
-.IP
-These shell command-line options are ignored to avoid nologin error.
-.IP "\fB\-h\fR, \fB\-\-help\fR"
-Display help text and exit.
-.IP "\fB\-V\fR, \fB\-\-version\fR"
-Display version information and exit.
-.SH NOTES
-.B nologin
-is a per-account way to disable login (usually used for system accounts like http or ftp).
-.BR nologin (8)
-uses /etc/nologin.txt as an optional source for a non-default message, the login
-access is always refused independently of the file.
-.PP
-.BR pam_nologin (8)
-PAM module usually prevents all non-root users from logging into the system.
-.BR pam_nologin (8)
-functionality is controlled by /var/run/nologin or the /etc/nologin file.
-.SH HISTORY
-The
-.B nologin
-command appeared in 4.4BSD.
-.SH AUTHORS
-.UR kzak@redhat.com
-Karel Zak
-.UE
-.SH SEE ALSO
-.BR login (1),
-.BR passwd (5),
-.BR pam_nologin (8)
-.SH AVAILABILITY
-The nologin command is part of the util-linux package and is available from
-.UR https://\:www.kernel.org\:/pub\:/linux\:/utils\:/util-linux/
-Linux Kernel Archive
-.UE .
