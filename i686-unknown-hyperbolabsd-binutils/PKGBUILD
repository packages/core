# Maintainer: André Silva <emulatorman@hyperbola.info>
# Maintainer: Márcio Silva <coadde@hyperbola.info>
# Contributor: Luke R. <g4jc@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

# Based on binutils package

_ctarget=i686-unknown-hyperbolabsd
pkgname=$_ctarget-binutils
pkgver=2.34
pkgrel=7
pkgdesc="A set of programs to assemble and manipulate binary and object files for HyperbolaBSD i686 target"
arch=('i686' 'x86_64')
url="https://www.gnu.org/software/binutils/"
license=('GPL-3' 'LGPL-3')
groups=('cross-devel')
depends=('zlib')
checkdepends=('dejagnu' 'bc')
options=('staticlibs' '!distcc' '!ccache')
source=(https://ftp.gnu.org/gnu/binutils/binutils-$pkgver.tar.lz{,.sig}
        binutils-hyperbolabsd.patch)
validpgpkeys=('3A24BC1E8FB409FA9F14371813FCEF89DD9E3C4F') # Nick Clifton <nickc@redhat.com>
sha512sums=('f4aadea1afa85d9ceb7be377afab9270a42ab0fd1fae86a7c69510b80de1aaac76f15cfb8730f9d233466a89fd020ab7e6e705e754c6b40f5fe2d16a5214562e'
            'SKIP'
            '92da2fe67434caa03e568ca590720d149e05fd642e6b0009e87f344c03c635de3a7da50b48d8e7d0644390762563001246fc52499b86e19e2d8d05ee4c88592c')

prepare() {
  cd binutils-$pkgver

  # hack! - libiberty configure tests for header files using "$CPP $CPPFLAGS"
  sed -i "/ac_cpp=/s/\$CPPFLAGS/\$CPPFLAGS -O2/" libiberty/configure

  # HyperbolaBSD patch
  patch -p1 -i ${srcdir}/binutils-hyperbolabsd.patch

  mkdir ${srcdir}/binutils-build
}

build() {
  cd binutils-build

  ../binutils-$pkgver/configure \
    --prefix=/usr \
    --libdir=/usr/$_ctarget/lib \
    --with-lib-path=/lib:/usr/lib:/usr/local/lib \
    --with-bugurl=https://issues.hyperbola.info/ \
    --enable-threads \
    --enable-ld=default \
    --enable-gold \
    --enable-plugins \
    --enable-deterministic-archives \
    --with-pic \
    --disable-werror \
    --disable-gdb \
    --disable-nls \
    --target=$_ctarget

  # check the host environment and makes sure all the necessary tools are available
  make configure-host

  make
}

check() {
  cd binutils-build

  # unset LDFLAGS as testsuite makes assumptions about which ones are active
  # ignore failures in gold testsuite...
  make -k LDFLAGS="" check || true
}

package() {
  cd binutils-build
  make prefix=${pkgdir}/usr libdir=${pkgdir}/usr/$_ctarget/lib install

  # Remove info documents that conflict with host version
  rm -rf ${pkgdir}/usr/share/info

  # install license files
  install -dm755 ${pkgdir}/usr/share/licenses/${pkgname}
  install -m644 ${srcdir}/binutils-$pkgver/COPYING3{,.LIB} ${pkgdir}/usr/share/licenses/${pkgname}
}
