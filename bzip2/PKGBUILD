# Maintainer (Arch): Ronald van Haren <ronald.archlinux.org>
# Contributor (Arch): Judd <jvinet@zeroflux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=bzip2
pkgver=1.0.8
_debver=1.0.8
_debrel=3
pkgrel=2
pkgdesc="A high-quality data compression program"
arch=('i686' 'x86_64')
license=('custom:bzip2')
url="http://www.bzip.org/"
groups=('base')
depends=('sh')
makedepends=('quilt')
source=(https://sourceware.org/pub/bzip2/$pkgname-$pkgver.tar.gz{,.sig}
        https://deb.debian.org/debian/pool/main/b/bzip2/bzip2_$_debver-$_debrel.debian.tar.bz2)
sha512sums=('083f5e675d73f3233c7930ebe20425a533feedeaaa9d8cc86831312a6581cefbe6ed0d08d2fa89be81082f2a5abdabca8b3c080bf97218a1bd59dc118a30b9f3'
            'SKIP'
            'd891e778d5b80ef64667d9094f0d37debe977365a655630434c64cb8eec3f2493ad92fc6d430ff8cdd7e518c30374cd36aaba9dc615129bcee82bf589686e43a')
validpgpkeys=('EC3CFE88F6CA0788774F5C1D1AA44BE649DE760A') # Mark Wielaard <mark@klomp.org>

prepare() {
  cd "$srcdir/$pkgname-$pkgver"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/20-legacy.patch || true

    quilt push -av
  fi

  # add large-file support
  sed -e 's/^CFLAGS=\(.*\)$/CFLAGS=\1 \$(BIGFILES)/' -i ./Makefile-libbz2_so

  # use our optimization
  sed -i "s|-O2|${CFLAGS}|g" Makefile
  sed -i "s|-O2|${CFLAGS}|g" Makefile-libbz2_so
}

build() {
  cd "$srcdir/$pkgname-$pkgver"

  make -f Makefile-libbz2_so
  make bzip2 bzip2recover
}

check() {
  cd "$srcdir/$pkgname-$pkgver"
  make test
}

package() {
  cd "$srcdir/$pkgname-$pkgver"

  install -dm755 $pkgdir/{bin,lib,usr/{include,lib,share/man/man1}}

  install -m755 bzip2-shared $pkgdir/bin/bzip2
  install -m755 bzip2recover bzdiff bzgrep bzmore $pkgdir/bin
  ln -sf bzip2 $pkgdir/bin/bunzip2
  ln -sf bzip2 $pkgdir/bin/bzcat

  install -m755 libbz2.so.$pkgver $pkgdir/lib
  ln -s libbz2.so.$pkgver $pkgdir/lib/libbz2.so.${pkgver::3}
  ln -s ../../lib/libbz2.so.${pkgver::3} $pkgdir/usr/lib/libbz2.so

  # this isn't the soname, and nothing should be looking for this file,
  # but leave it alone for now
  ln -s libbz2.so.$pkgver $pkgdir/lib/libbz2.so.${pkgver::1}

  install -m644 bzlib.h $pkgdir/usr/include/

  install -m644 bzip2.1 $pkgdir/usr/share/man/man1/
  ln -sf bzip2.1 $pkgdir/usr/share/man/man1/bunzip2.1
  ln -sf bzip2.1 $pkgdir/usr/share/man/man1/bzcat.1
  ln -sf bzip2.1 $pkgdir/usr/share/man/man1/bzip2recover.1

  install -Dm644 $srcdir/${pkgname}-${pkgver}/LICENSE \
                 $pkgdir/usr/share/licenses/${pkgname}/LICENSE
}
