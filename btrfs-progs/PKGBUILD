# Maintainer (Arch): Sébastien "Seblu" Luttringer <seblu@archlinux.org>
# Contributor (Arch): Tom Gundersen <teg@jklm.no>
# Contributor (Arch): Tobias Powalowski <tpowa@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=btrfs-progs
pkgver=5.9
pkgrel=4
pkgdesc="Btrfs filesystem utilities"
license=('GPL-2')
arch=('i686' 'x86_64')
url='https://btrfs.wiki.kernel.org'
replaces=('btrfs-progs-unstable')
conflicts=('btrfs-progs-unstable')
provides=('btrfs-progs-unstable')
depends=('e2fsprogs' 'lzo')
makedepends=('asciidoc' 'xmlto' 'python-setuptools')
optdepends=('python: libbtrfsutil python bindings')
source=(https://www.kernel.org/pub/linux/kernel/people/kdave/${pkgname}/${pkgname}-v${pkgver}.tar.{xz,sign}
        initcpio-install-btrfs
        initcpio-hook-btrfs)
install="${pkgname}.install"
options=('!staticlibs')
sha512sums=('38db047198ab0467d3fe235f7e8e1fd14ade00dd0b182588220c5f5e4b598efbdcd30a3d5d56cf0a0ed8a902d2374af0927eb7f3720cd761172c7291a2a65c94'
            'SKIP'
            'c0222069e1b8f0a31e8752d986b3f3d710b923cd0d9045ef9e762e8555c771667f1e6cbe71bc4e5b78cb1570f96402e3d545a8192343486d53469a9ec634700e'
            '2cd4df9154555d5ffaf5279b5d730659fe1f15cf103fc222303f5fec14d8ccf7ac97cb88a826e0b43f466f779ecb49f562b7ac6425734b7e5cb8ff691480aa6c')
validpgpkeys=('F2B41200C54EFB30380C1756C565D5F9D76D583B') # David Sterba <dsterba@suse.cz>

build() {
  cd "${srcdir}/${pkgname}-v${pkgver}"
  ./configure \
    --prefix=/usr \
    --bindir=/sbin \
    --disable-zstd
  make
}

check() {
  cd "${srcdir}/${pkgname}-v${pkgver}"
 ./btrfs filesystem show
}

package() {
  cd "${srcdir}/${pkgname}-v${pkgver}"
  make DESTDIR="${pkgdir}" 'install' 'install_python'

  # install license
  install -Dm '644' 'COPYING' -t "${pkgdir}/usr/share/licenses/${pkgname}"

  # install bash completion (FS#44618)
  install -Dm '644' 'btrfs-completion' \
    "${pkgdir}/usr/share/bash-completion/completions/btrfs"

  # install mkinitcpio hooks
  install -Dm '644' "${srcdir}/initcpio-install-btrfs" \
    "${pkgdir}/lib/initcpio/install/btrfs"
  install -Dm '644' "${srcdir}/initcpio-hook-btrfs" \
    "${pkgdir}/lib/initcpio/hooks/btrfs"
}
