# Maintainer: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=vlock
pkgver=2.2.2
_debver=$pkgver
_debrel=10
pkgrel=2
pkgdesc="Virtual Console locking program"
arch=('i686' 'x86_64')
url='https://packages.debian.org/source/bullseye/$pkgname'
license=('GPL-2')
depends=('shadow')
makedepends=('quilt')
source=("https://deb.debian.org/debian/pool/main/v/$pkgname/${pkgname}_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/v/$pkgname/${pkgname}_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('694da52fba3a73f342c4217f6e3556e8c49cab6a991f0a3b96f6adfdb4a718e8e66ce9b5cc83f74475675a9d31d3956545acd11decf9e3b2db61df53ae863b52'
            '5aabc89f111f6a08f868aa2d3dd37fd7a09a8f0245850cccd7a1c1f2bbc3eeeb13c45222f4cfd6bf7f3a35d1dce910ed53103aae27da16195185a88597247499')

prepare() {
  cd ${srcdir}/${pkgname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd ${srcdir}/${pkgname}-${pkgver}
  ./configure \
    VLOCK_GROUP=root \
    --prefix=/usr \
    --enable-shadow
  sed -i 's/new\.so//' config.mk
  make
}

package() {
  cd ${srcdir}/${pkgname}-${pkgver}

  # current makefile contains errors
  make DESTDIR=${pkgdir} install

  # fix modules permissions
  chmod 0755 ${pkgdir}/usr/lib/vlock/modules/*.so

  # license
  install -Dm644 COPYING -t ${pkgdir}/usr/share/licenses/${pkgname}
}
