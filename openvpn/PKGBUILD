# Maintainer (Arch): Christian Hesse <mail@eworm.de>
# Contributor (Artix): artoo <artoo@cromnix.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=openvpn
pkgver=2.5.1
_debver=$pkgver
_debrel=3
pkgrel=1
pkgdesc="An easy-to-use, robust and highly configurable VPN (Virtual Private Network)"
arch=('i686' 'x86_64')
url='https://openvpn.net/index.php/open-source.html'
depends=('libressl' 'lzo' 'iproute2' 'libeudev' 'pkcs11-helper')
optdepends=('easy-rsa: easy CA and certificate handling'
            'logger: message logging support')
makedepends=('eudev' 'quilt')
backup=('etc/conf.d/openvpn-client'
        'etc/conf.d/openvpn-server'
        'etc/sv/openvpn-client/conf'
        'etc/sv/openvpn-server/conf')
license=('custom:GPL-2+OpenSSL-Linking-Exception')
source=("https://swupdate.openvpn.net/community/releases/${pkgname}-${pkgver}.tar.xz"{,.asc}
        "https://deb.debian.org/debian/pool/main/o/openvpn/openvpn_$_debver-$_debrel.debian.tar.xz"
        'openvpn-client.confd'
        'openvpn-server.confd'
        'openvpn-client.initd'
        'openvpn-server.initd'
        'openvpn-client.run'
        'openvpn-server.run'
        'openvpn.modulesd')
sha512sums=('7c0adad384f908bd7dbd839a2b90cbe3a4222cac92ef484df89709ca5dd6cb22b3caf19b696c2bb74d7eda148904a8b25f1fe4640c91f0e68d6e65bcf922e0f4'
            'SKIP'
            '1b103c5f3a48e391afa6a64a624238e70e3f624eb089b3611f4f5df47370903a573e296735347be14cb422828013cfeaf2009782f11a756ceee37b43c73c05a2'
            '369dc2968aeeba7757784b2ab950f261cdcdaf79c16b96ebe8c16c51c21e7cc38e80c31e00a0a51299d5c27137fe806de42b22ffd1cdf3b0da9eb799c82852aa'
            'a08ec66868369013013dd880672eabd6fe2a1d19ff03003cac2b4cb91b3e07955080f79cbaa556c5556a1153664b12388a42d47caf8828e7632c5125c6404935'
            '56e792d0105b1e048093086e277104d3ec25d74a8d2e9a19f5ab49287dc188fab5ecc96254c1025654c4cb620e1621f1117142039394d8049fefafb4d9ff5f6f'
            'c9043822ea016cb166fe238089b5729c4f6f787d44f3f0a8b619faafb9e9e7a5efc91214bb6055db645385882aec471cb0584200361611f7c489755db49c7b3d'
            '63dc9d2c14677159b88b1a6af4c6d61e2ea3dbc574f9dec0b703445167b9489e92010d5a301dd6fc62e1e79c27a1b39ee5982691811d0a6f1fe7ec7115100480'
            'f024683deeb037815dd3fd82db75c93cd85157d21ee3f813f715bf6246c97af124e3addc7e0899a70e1294001713848ab5703ce253410c887ea181464397b648'
            '871b650ff982be061532b4c5fe4812f41e6e2c58fc69b24f8a745f9a43389da44e742a69b0467b3c3d9e2b031af0728e20f10fa4584695c4f5ac87768a1fd34e')
validpgpkeys=('F554A3687412CFFEBDEFE0A312F5F7B42F2B01E7') # OpenVPN - Security Mailing List <security@openvpn.net>

prepare() {
  cd "${srcdir}"/${pkgname}-${pkgver}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/debian_nogroup_for_sample_files.patch || true
    rm -v debian/patches/systemd.patch || true

    quilt push -av
  fi

  # regenerate configure script
  autoreconf -fi
}

build() {
  cd "${srcdir}"/${pkgname}-${pkgver}

  ./configure \
    --prefix=/usr \
    --enable-iproute2 \
    --enable-pkcs11 \
    --enable-plugins \
    --disable-systemd \
    --enable-x509-alt-username \
    --disable-plugin-auth-pam
  make
}

package() {
  cd "${srcdir}"/${pkgname}-${pkgver}

  # Install openvpn
  make DESTDIR="${pkgdir}" install

  # Create empty configuration directories
  install -d -m0750 -g 90 "${pkgdir}"/etc/openvpn/{client,server}

  # Install examples
  install -d -m0755 "${pkgdir}"/usr/share/openvpn
  cp -r sample/sample-config-files "${pkgdir}"/usr/share/openvpn/examples

  # Install license
  install -d -m0755 "${pkgdir}"/usr/share/licenses/openvpn/
  ln -sf /usr/share/doc/openvpn/{COPYING,COPYRIGHT.GPL} "${pkgdir}"/usr/share/licenses/openvpn/

  # Install contrib
  for FILE in $(find contrib -type f); do
    case "$(file --brief --mime-type "${FILE}")" in
      "text/x-shellscript") install -D -m0755 "${FILE}" "${pkgdir}/usr/share/openvpn/${FILE}" ;;
      *) install -D -m0644 "${FILE}" "${pkgdir}/usr/share/openvpn/${FILE}" ;;
    esac
  done

  # Install OpenRC files
  for f in openvpn-{client,server}; do
    install -Dm644 "${srcdir}"/$f.confd "${pkgdir}"/etc/conf.d/$f
    install -Dm755 "${srcdir}"/$f.initd "${pkgdir}"/etc/init.d/$f
  done

  # Install runit files
  for f in openvpn-{client,server}; do
    install -Dm644 "${srcdir}"/$f.confd "${pkgdir}"/etc/sv/$f/conf
    install -Dm755 "${srcdir}"/$f.run "${pkgdir}"/etc/sv/$f/run
  done

  install -Dm644 "${srcdir}"/openvpn.modulesd "${pkgdir}"/etc/modules.d/openvpn
}
