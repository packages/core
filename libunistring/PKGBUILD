# Maintainer (Arch):  Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor (Arch): Jan de Groot <jgc@archlinux.org>
# Contributor (Arch): Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor (Arch): Emmanuel 'guinness' Boudreault
# Contributor (Arch): Patrick McCarty <pnorcks at gmail dot com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Jesús E. <heckyel@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=libunistring
pkgver=0.9.10
_debver=$pkgver
_debrel=4
pkgrel=2
pkgdesc='Library for manipulating Unicode strings and C strings'
url='https://www.gnu.org/software/libunistring/'
arch=('i686' 'x86_64')
license=('LGPL-3')
depends=('glibc')
makedepends=('quilt')
source=("https://ftp.gnu.org/gnu/${pkgname}/${pkgname}-${pkgver}.tar.xz"
        "https://deb.debian.org/debian/pool/main/libu/libunistring/libunistring_${_debver}-${_debrel}.debian.tar.xz")
sha512sums=('01dcab6e05ea4c33572bf96cc0558bcffbfc0e62fc86410cef06c1597a0073d5750525fe2dee4fdb39c9bd704557fcbab864f9645958108a2e07950bc539fe54'
            'b687df5ffae03ad5de8c2ee42b566946c2164574a78801a55ca1a4ee61d602007de002f5c996b7b408bf8793706061b6339657b50db305811e543e24de87516e')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  autoreconf -vfi
}

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr
  make
}

package() {
  make -C $pkgname-$pkgver DESTDIR="$pkgdir" install

  # install license file
  install -Dm644 "$pkgname-$pkgver/COPYING.LIB" -t "$pkgdir/usr/share/licenses/$pkgname"
}
