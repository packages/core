# Maintainer (Arch): Lukas Fleischer <lfleischer@archlinux.org>
# Contributor (Arch): Allan McRae <allan@archlinux.org>
# Contributor (Arch): Eric Belanger <eric@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=bison
pkgver=3.7.5
_debver=3.7.5+dfsg
_debrel=1
pkgrel=3
pkgdesc="The GNU general-purpose parser generator"
arch=('i686' 'x86_64')
license=('GPL-3')
url="https://www.gnu.org/software/bison/bison.html"
depends=('m4')
makedepends=('quilt')
groups=('base-devel')
replaces=('bison-legacy')
conflicts=('bison-legacy')
provides=('bison-legacy')
source=("https://ftp.gnu.org/gnu/bison/${pkgname}-${pkgver}.tar.xz"{,.sig}
        "https://deb.debian.org/debian/pool/main/b/bison/bison_$_debver-$_debrel.debian.tar.xz"
        'remove-java-support.patch')
sha512sums=('98cdfaf114b8f8eb0927b29fe999dc9629336333d85bd2f87c4c558125500c44ee6fbfff453e3121c7e9e239a632f8c72e08c39be7dfb045361d35ec59d31811'
            'SKIP'
            '53fec70e646b110b686351df9585540459d528625b98fcddb4d0a3d31990ec2b094a4d99827eb49f464f3dc5620b192498a2772abcf2e60d0b6a886dec1ba5b1'
            'c80fcb55b41bbc29b74d90051cf024a3f731755e24ef514ff9e757b18624e102e2099bacd288e26cf35c90467437af590c599b3a21ab8361ec174d173bff1c91')
validpgpkeys=('7DF84374B1EE1F9764BBE25D0DDCAA3278D5264E')  # Akim Demaille

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  if [[ ${pkgver%.*} = ${_debver%.*.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  patch -p1 -i ../remove-java-support.patch
  libtoolize --force
  aclocal
  autoheader
  automake --force-missing --add-missing
  autoconf
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  ./configure --prefix=/usr --datadir=/usr/share --disable-yacc
  make
}

check() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  # do not abort on error as some are "expected"
  make check || true
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install
  install -Dm644 COPYING "${pkgdir}/usr/share/licenses/${pkgname}/COPYING"
}
