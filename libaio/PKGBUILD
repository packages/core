# Maintainer (Arch): Bartłomiej Piotrowski <barthalion@gmail.com>
# Contributor (Arch): Thomas S Hatch <thatch45 at gmail dot com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=libaio
pkgver=0.3.112
_debver=0.3.112
_debrel=8
pkgrel=2
pkgdesc="Kernel asynchronous I/O (AIO) support"
arch=('i686' 'x86_64')
url="http://lse.sourceforge.net/io/aio.html"
license=('LGPL-2.1')
depends=('glibc')
makedepends=('quilt')
source=(https://deb.debian.org/debian/pool/main/liba/libaio/libaio_$_debver.orig.tar.xz
        https://deb.debian.org/debian/pool/main/liba/libaio/libaio_$_debver-$_debrel.debian.tar.xz)
sha512sums=('612f0adeea4926ced914b43ec550bf821bef348e6c787f9e3c1a545e5667121692a9af7ebaab14744aca7132a071083a1120bd01e31726932f3ceb9be51891a7'
            '1f1ec3720fca8a6a80481b85eb86dd891479087b03acfb747ca0f300f70bfd03a729e4d8a3fc59d0b874b979cb2bbfc30f127ee29558aa6b5d2bc86018fd4590')

prepare() {
  cd "$srcdir/$pkgname-$pkgver"
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "$srcdir/$pkgname-$pkgver"
  # AIO library is a thin wrapper around kernel syscalls, it does not use stdlib
  # and other helpers like stack protection libraries
  CFLAGS="-march=${CARCH/_/-} -mtune=generic -O2 -pipe"
  make
}

check() {
  cd "$srcdir/$pkgname-$pkgver"

  if [[ ${pkgver%.*} != ${_debver%.*} ]]; then
    # work around gcc warning (-Werror) in test suite
    sed -i '/strncpy/s#sizeof(TEMPLATE)#sizeof(template)#' harness/cases/19.t
    sed -i '/strncpy/s#sizeof(TEMPLATE)#sizeof(temp_file)#' harness/cases/21.t
  fi

  sed -i '\|define FAIL 1| s|1|3|' harness/cases/20.t

  make partcheck
}

package() {
  cd "$srcdir/$pkgname-$pkgver"
  make DESTDIR="$pkgdir" prefix="/usr" libdir="/lib" install
  install -Dm644 COPYING $pkgdir/usr/share/licenses/$pkgname/COPYING
}
